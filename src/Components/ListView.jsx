import React from 'react'
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import CardMedia from '@material-ui/core/CardMedia';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import ImageModel from './ImageModel';

export default class ListView extends React.Component {

    render() {
        return (
            <Container style={{ paddingTop: '2px', paddingBottom: '4px' }} maxWidth="md">

                {this.props.list.map((card) => (
                    <Card style={{
                        height: '100%',
                        display: 'flex',
                        flexDirection: 'row',
                        marginBottom: '2%'
                    }}>
                        <CardContent style={{ flexGrow: 1 }}>
                            <Grid container spacing={2}>
                                <Grid item xs={4}>
                                    {(this.props.loading) ? (
                                        <img src="https://getuikit.com/v2/docs/images/placeholder_200x100.svg" alt="Thubnail Image" />
                                    ) : (
                                        <CardMedia
                                            style={{ paddingTop: '56.25%', }}
                                            image={card.download_url + (this.props.grayScale ? "?grayscale" : "") + (this.props.blur !== 0 ? "?blur=" + this.props.blur : "")}
                                            title={card.author}
                                        />
                                    )}
                                </Grid>
                                <Grid item xs={8}>
                                    <Typography gutterBottom variant="h5" component="h2">
                                        {card.author}
                                    </Typography>
                                    <Typography>
                                        This is a media card. You can use this section to describe the content.
                                    </Typography>
                                    <div style={{ textAlign: 'end', marginTop: '30px' }}>
                                        <ImageModel card={card} />
                                    </div>
                                </Grid>
                            </Grid>
                        </CardContent>
                    </Card>

                ))
                }
            </Container>
        )
    }
}