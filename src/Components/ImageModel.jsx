import React from 'react'
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import Modal from '@material-ui/core/Modal';
import CardMedia from '@material-ui/core/CardMedia';
import ZoomInIcon from '@material-ui/icons/ZoomIn';
import ZoomOutIcon from '@material-ui/icons/ZoomOut';
import CloudDownloadIcon from '@material-ui/icons/CloudDownload';
const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    bgcolor: 'background.paper',
    boxShadow: 24,
    padding: '4%',
};

export default class ImageModel extends React.Component {
    state = {
        displayModel: false,
        card: this.props.card,
        imageHeight: 250,
        imageWidth: 400
    }
    render() {
        return (
            <div>
                <Button onClick={() => { this.setState({ displayModel: true }) }}>Open modal</Button>
                <Modal
                    open={this.state.displayModel}
                    onClose={() => { this.setState({ displayModel: false }) }}
                    aria-labelledby="modal-modal-title"
                    aria-describedby="modal-modal-description"
                >
                    <Box sx={style}>
                        <Typography id="modal-modal-title" variant="h6" component="h2">
                            {this.state.card.author}
                        </Typography>
                        <hr />
                        <CardMedia
                            style={{ paddingTop: '56.25%', }}
                            image={this.state.card.download_url}
                            title={this.state.card.author}
                            style={{ width: this.state.imageWidth, height: this.state.imageHeight }}
                        />
                        <hr />
                        <Grid container>
                            <Grid item xs={12} style={{ textAlign: 'center' }}>
                                <ZoomInIcon onClick={() => {
                                    this.setState({
                                        imageHeight: this.state.imageHeight + 10,
                                        imageWidth: this.state.imageWidth + 10
                                    })
                                }} />
                                <ZoomOutIcon onClick={() => {
                                    this.setState({
                                        imageHeight: this.state.imageHeight - 10,
                                        imageWidth: this.state.imageWidth - 10
                                    })
                                }} />
                                <CloudDownloadIcon onClick={() => {
                                    var link = document.createElement("a");
                                    link.download = "test.jpg";
                                    var binaryData = [];
                                    binaryData.push(this.state.card.download_url);
                                    link.href = window.URL.createObjectURL(new Blob(binaryData, { type: "application/image" }))
                                    // link.href = URL.createObjectURL(this.state.card.download_url + ".jpg");
                                    link.click();
                                    // window.location.href = this.state.card.download_url + ".jpg"
                                }} />
                            </Grid>
                        </Grid>
                    </Box>
                </Modal>
            </div >
        )
    }
}