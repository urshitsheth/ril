import React from 'react'
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Grid from '@material-ui/core/Grid';
import ImageModel from './ImageModel';
export default class GridView extends React.Component {
    render() {
        return (
            <Container style={{ paddingTop: '2px', paddingBottom: '4px' }} maxWidth="md">
                <Grid container spacing={4}>
                    {this.props.list.map((card) => (
                        <Grid item key={card} xs={12} sm={6} md={4}>
                            <Card style={{
                                height: '100%',
                                display: 'flex',
                                flexDirection: 'column',
                            }}>
                                {(this.props.loading) ? (
                                    <img src="https://getuikit.com/v2/docs/images/placeholder_200x100.svg" alt="Thubnail Image" />
                                ) : (
                                    <CardMedia
                                        style={{ paddingTop: '56.25%', }}
                                        image={card.download_url + (this.props.grayScale ? "?grayscale" : "") + (this.props.blur !== 0 ? "?blur=" + this.props.blur : "")}
                                        title={card.author}
                                    />
                                )}

                                <CardContent style={{ flexGrow: 1 }}>
                                    <Typography gutterBottom variant="h5" component="h2">
                                        {card.author}
                                    </Typography>
                                    <Typography>
                                        This is a media card. You can use this section to describe the content.
                                    </Typography>
                                </CardContent>
                                <CardActions>
                                    <div style={{ textAlign: 'end', marginTop: '30px' }}>
                                        <ImageModel card={card} />
                                    </div>
                                </CardActions>
                            </Card>
                        </Grid>
                    ))}
                </Grid>
            </Container>
        )
    }
}