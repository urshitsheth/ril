import React from 'react';
import logo from './logo.svg';
import './App.css';
import MainContent from './Pages/MainContent';

function App() {
  return (
    <React.Fragment>
      <MainContent />
    </React.Fragment>
  );
}

export default App;
