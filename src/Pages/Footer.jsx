import React from 'react'
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Link from '@material-ui/core/Link';
import Container from '@material-ui/core/Container';

function Copyright() {
    return (
        <Typography variant="body2" color="textSecondary" align="center">
            {'Copyright © '}
            <Link color="inherit" href="https://mui.com/">
                https://thecodehackers.com/
            </Link>{' '}
            {new Date().getFullYear()}
            {'.'}
        </Typography>
    );
}

export default class Footer extends React.Component {
    render() {
        return (
            <Container maxWidth="sm" style={{ padding: '2%' }}>
                {/* Footer */}
                <Copyright />
                {/* End footer */}
            </Container>
        )
    }
}