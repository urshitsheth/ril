import React from 'react'
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Switch from '@material-ui/core/Switch';
import ViewListIcon from '@material-ui/icons/ViewList';
import AppsIcon from '@material-ui/icons/Apps';
import GridView from '../Components/GridView'
import ListView from '../Components/ListView'
import Slider from '@material-ui/core/Slider'
import Box from '@material-ui/core/Box';


export default class Content extends React.Component {
    state = {
        list: [],
        toggleButton: false,
        page: 1,
        limit: 6,
        grayScale: false,
        blur: 0,
        loading: false
    }

    componentDidMount() {
        this.loadImages()
        window.onscroll = (ev) => {
            if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
                this.setState({ page: this.state.page + 1, loading: true }, () => this.loadImages())

            }
        };
    }

    loadImages = () => {
        const { page, limit } = this.state
        fetch("https://picsum.photos/v2/list?page=" + page + "&limit=" + limit).then(result => {
            if (result.ok) {
                return result.json().then(res => {

                    this.setState({
                        list: [...this.state.list, ...res],
                        loading: false
                    })
                })
            }
        })
    }


    render() {
        return (
            <>
                <Grid container spacing={2} alignItems="center" justifyContent="center" style={{ position: 'fixed', height: '5em', backgroundColor: '#FFFFFF', top: '65px', zIndex: 1 }}>
                    <Grid item >
                        <ViewListIcon title="ListView" onClick={() => { this.setState({ toggleButton: true }) }} />
                    </Grid>
                    <Grid item>
                        <AppsIcon title="GridView" onClick={() => { this.setState({ toggleButton: false }) }} />
                    </Grid>
                    <Grid item>
                        <Switch label="Greyscale" title="Greyscale" onClick={() => {
                            this.setState({ grayScale: !this.state.grayScale })
                        }} />
                    </Grid>
                    <Grid item>
                        <Box sx={{ width: 100 }}>
                            <Slider defaultValue={0} onChange={(event, newValue) => {
                                this.setState({ blur: newValue })
                            }} step={1} min={0} max={10} />
                        </Box>

                    </Grid>
                </Grid>
                <main>
                    <Container style={{ marginTop: '10%' }}>
                        {
                            (this.state.toggleButton) ? (
                                <ListView
                                    list={this.state.list}
                                    grayScale={this.state.grayScale}
                                    blur={this.state.blur}
                                    loading={this.state.loading}
                                />
                            ) : (
                                <GridView
                                    list={this.state.list}
                                    grayScale={this.state.grayScale}
                                    blur={this.state.blur}
                                    loading={this.state.loading}
                                />
                            )
                        }
                    </Container>
                </main>
            </>

        )
    }
}